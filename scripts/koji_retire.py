#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier:      GPL-2.0
# Author: Mohan Boddu <mboddu@redhat.com>



# Script to untag, block and remove the pkg in stream koji when it gets retired.


import koji
import argparse
import yaml

parser = argparse.ArgumentParser()
parser.add_argument('-p','--packages', metavar='package', nargs="+", help='Packages to retire', required=True)
args = parser.parse_args()
pkgs = args.packages

# Create a koji session
koji_module = koji.get_profile_module("stream")
kojisession = koji_module.ClientSession(koji_module.config.server)

# Log into koji
kojisession.gssapi_login()

tags = ['c9s-pending', 'c9s-gate', 'c9s-build']
for pkg in pkgs:
    for tag in tags:
        builds = kojisession.listTagged(tag, package=pkg)
        for build in builds:
            nvr = build['nvr']
            print('Untagging {0} from {1} tag'.format(nvr, tag))
            kojisession.untagBuild(tag,nvr)
    try:
        print('Blocking {0} from c9s tag'.format(pkg))
        kojisession.packageListBlock('c9s',pkg)
    except:
        print('{0} package is not in c9s tag'.format(pkg))
